Creates and provisions a docker swarm cluster with 1 to N nodes.
It uses the generic/alpine38 box as base

The number of nodes and the base box are configurable through enviromental variables

Warning: It copies the host's public key to the guest FS


requirements:
 - virtualbox
 - vagrant

Run 
- `cp ./.env.example ./.env`
- Edit the .env 
- vagrant up


Docker swarm:
- Create a `hosts` file at the project's root
- Use the ansible playbooks to creat the docker swarm cluster

hosts file: 
master node ip: 192.168.50.10

worker node ip: 192.168.50.(10 + i) # i=0, every worker node i += 1. i.e. node1 =192.168.50.11, node2 192.168.50.12 etc
