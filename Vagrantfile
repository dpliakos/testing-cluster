Vagrant.configure("2") do |config|
  config.ssh.insert_key = false
  config.env.enable

  USER = ENV['USE_USER'].nil? ? ENV['USER'] : ENV['USE_USER']
  IMAGE_NAME = ENV['BASE_BOX'].nil? ? "generic/alpine38" : ENV['BASE_BOX']
  CLUSTER = !ENV['CLUSTER'].nil? && ENV['CLUSTER'] == "true"
  N = ENV['CLUSTER'] && ENV['NODES'].to_i > 0 ? ENV['NODES'].to_i : 0

  if N <= 0
    USE_CLUSTER = false
  end

  config.vm.define "master" do |master|
    master.vm.box = IMAGE_NAME
    master.vm.network "private_network", ip: "192.168.50.10"
    master.vm.hostname = "master"
    master.disksize.size = '10GB'
    
    master.vm.provider "virtualbox" do |v|
      v.memory = 1024
      v.cpus = 2
    end

    master.vm.provision "file", source: "/home/#{USER}/.ssh/id_rsa.pub", destination: "/tmp/user_key.pub"
    master.vm.provision "shell", inline: <<-SHELL
      cat /tmp/user_key.pub >> /home/vagrant/.ssh/authorized_keys
      mkdir -vp /root/.ssh
      cat /tmp/user_key.pub >> /root/.ssh/authorized_keys
    SHELL
  end

  if CLUSTER 
    (1..N).each do |i|
      config.vm.define "node-#{i}" do |node|
        node.vm.box = IMAGE_NAME
        node.vm.network "private_network", ip: "192.168.50.#{i + 10}"
        node.vm.hostname = "node-#{i}"
        node.disksize.size = '10GB'
        
        node.vm.provider "virtualbox" do |v|
          v.memory = 1024
          v.cpus = 1
        end

        node.vm.provision "file", source: "/home/#{USER}/.ssh/id_rsa.pub", destination: "/tmp/user_key.pub"
        node.vm.provision "shell", inline: <<-SHELL
          cat /tmp/user_key.pub >> /home/vagrant/.ssh/authorized_keys
          mkdir -vp /root/.ssh
          cat /tmp/user_key.pub >> /root/.ssh/authorized_keys
        SHELL
      end
    end
  end
end
